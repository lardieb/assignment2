

BallPowerup = Class{__includes = Powerup}

function BallPowerup:init(x, y, cb)
	Powerup.init(self, PowerupType.EXTRA_BALL, x, y, cb)
end