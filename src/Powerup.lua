--[[
    GD50
    Breakout Remake

    -- Powerup Class --

    Author: Brant Lardie
    edx@lardie.net

    Represents a powerup
]]

Powerup = Class{}

function Powerup:init(skin, x, y, cb)
    -- simple positional and dimensional variables
    self.width = 16
    self.height = 16

    self.dy = math.random(10,40)

    self.x = x-8
    self.y = y-8

    self.skin = skin

    self.callback = cb
end

--[[
    Expects an argument with a bounding box,
    and returns true if the bounding boxes of this and the argument overlap.
]]
function Powerup:collides(target)
    -- first, check to see if the left edge of either is farther to the right
    -- than the right edge of the other
    if self.x > target.x + target.width or target.x > self.x + self.width then
        return false
    end

    -- then check to see if the bottom edge of either is higher than the top
    -- edge of the other
    if self.y > target.y + target.height or target.y > self.y + self.height then
        return false
    end 

    -- if the above aren't true, they're overlapping
    return true
end

function Powerup:update(dt)
    self.y = self.y + self.dy * dt
end

function Powerup:render()
    -- gTexture is our global texture for powerups
    -- gFrames is a table of quads mapping the individual powerups in the texture

    love.graphics.draw(gTextures['main'], gFrames['powerups'][self.skin],
        self.x, self.y)
end

function Powerup:callCallback(obj)
    if self.callback ~= nil then
        self.callback(obj, self)
    end
end